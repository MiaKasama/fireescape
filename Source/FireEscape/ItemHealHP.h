// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealHP.h"
#include "ItemHealHP.generated.h"

/**
 * 
 */
UCLASS()
class FIREESCAPE_API AItemHealHP : public AHealHP
{
	GENERATED_BODY()
	
public:

	AItemHealHP();

	FORCEINLINE float GetItem() const { return HealItem; }
	void SetItem(float newItem) { HealItem = newItem; }

	void WasHealed_Implementation() override;

protected:

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HealItem")
		float HealItem;
};
