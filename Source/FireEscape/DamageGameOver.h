// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DamageGameOver.generated.h"

UCLASS()
class FIREESCAPE_API ADamageGameOver : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADamageGameOver();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// レベル上で強制ゲームオーバーにするメッシュ
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GameOver", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* GameOverMesh;

public:
	// ゲームオーバーメッシュを返す
	FORCEINLINE UStaticMeshComponent* GetOverMesh() const { return GameOverMesh; };

	// これがゲームオーバーにするかどうかの確認用
	UFUNCTION(BlueprintPure, Category = "GameOver")
		bool IsOverActive();

	// ゲームオーバーにするかどうかを設定する
	UFUNCTION(BlueprintCallable, Category = "GameOver")
		void SetOverActive(bool NewOverState);

protected:
	// これがゲームオーバーにするかどうか
	bool bIsOverActive;
	
};
