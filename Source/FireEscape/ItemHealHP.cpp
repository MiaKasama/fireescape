// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemHealHP.h"

AItemHealHP::AItemHealHP()
{
	GetHealMesh()->SetSimulatePhysics(true);
	SetItem(0.5f);
}

void AItemHealHP::WasHealed_Implementation()
{
	Super::WasHealed_Implementation();

	UE_LOG(LogTemp, Warning, TEXT("C++ called"))

	Destroy();
}
