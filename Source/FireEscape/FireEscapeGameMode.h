// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FireEscapeGameMode.generated.h"

// ゲームステート
UENUM(BlueprintType)
enum class EFirePlayState : uint8
{
	EPlaying,  // ゲームプレイ中
	EGameOver, // ゲームオーバー
	EClear,    // ゲームクリア
	EUnknown   // 分からない
};

UCLASS(minimalapi)
class AFireEscapeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFireEscapeGameMode();
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Health")
		float GetHPToOver() const;

	UFUNCTION(BlueprintPure, Category = "Health")
		EFirePlayState GetCurrentState() const;

	void SetCurrentState(EFirePlayState NewState);
	
	UFUNCTION(BlueprintPure, Category = "FinalTime")
		float GetFinalTimeDecimal2() const;

	UFUNCTION(BlueprintPure, Category = "FinalTime")
		float GetFinalTimeDecimal1() const;

	UFUNCTION(BlueprintPure, Category = "FinalTime")
		float GetFinalTimeSecond() const;

	UFUNCTION(BlueprintPure, Category = "FinalTime")
		float GetFinalTimeMinute() const;

protected:

	//炎を浴びている間に減少するHP
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (BlueprintProtected = "true"))
		float FireRate;

	// ゲームオーバーする時のHP
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (BlueprintProtected = "true"))
		float HPToOver;

	// ゲームクリアする時の位置
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Position", meta = (BlueprintProtected = "true"))
		float ClearX;

	// ゲームクリアする時の位置
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Position", meta = (BlueprintProtected = "true"))
		float ClearY;

	// ゲームクリアする時の位置
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Position", meta = (BlueprintProtected = "true"))
		float ClearZ;

	// HUD画面に必要なWidgetクラス
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> HUDWidgetClass;

	// HUDのインスタンス
	UPROPERTY()
		class UUserWidget* CurrentWidget;

	// 最終時間（ゲーム終了時の時間経過）
	// 最終時間経過(コンマ)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FinalTime", meta = (BlueprintProtected = "true"))
		int FinalTimeDecimal2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FinalTime", meta = (BlueprintProtected = "true"))
		int FinalTimeDecimal1;

	// 最終時間経過(秒)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FinalTime", meta = (BlueprintProtected = "true"))
		int FinalTimeSecond;

	// 最終時間経過(分)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FinalTime", meta = (BlueprintProtected = "true"))
		int FinalTimeMinute;

	// 最終時間チェック
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FinalTime", meta = (BlueprintProtected = "true"))
		bool FinalTimeCheck;

		
private:
	// 現在のゲームのステート
	EFirePlayState CurrentState;

	// ステートに応じて処理を変えるためのハンドラ
	void HandleNewState(EFirePlayState NewState);
};



