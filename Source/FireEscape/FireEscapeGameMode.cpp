// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FireEscapeGameMode.h"
#include "FireEscapeCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

AFireEscapeGameMode::AFireEscapeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// Tick関数を有効にする
	PrimaryActorTick.bCanEverTick = true;
}

void AFireEscapeGameMode::BeginPlay()
{
	Super::BeginPlay();

	// ステートをPlayingにする
	SetCurrentState(EFirePlayState::EPlaying);

	// ゲームオーバー時のHPを設定(基本0)
	// ゲームクリア時の位置座標を設定
	AFireEscapeCharacter* MyCharacter = Cast<AFireEscapeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		HPToOver = 0.0f;

		ClearX = 1500.0f;
		ClearY = -1500.0f;
		ClearZ = 10000.0f;
	}

	FinalTimeCheck = false;

	// HUDの追加
	if (HUDWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

void AFireEscapeGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// キャラクターの取得
	AFireEscapeCharacter* MyCharacter = Cast<AFireEscapeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		// HP0でステートをゲームオーバーに
		if (MyCharacter->GetCharacterHP() <= HPToOver)
			SetCurrentState(EFirePlayState::EGameOver);

		// ある位置に着いたらゲームクリアに
		else if (MyCharacter->GetCharacterZ() >= ClearZ && MyCharacter->GetCharacterY() >= ClearY && MyCharacter->GetCharacterX() >= ClearX)
			SetCurrentState(EFirePlayState::EClear);

		else
			SetCurrentState(EFirePlayState::EPlaying);
	}
}

float AFireEscapeGameMode::GetHPToOver() const
{
	return HPToOver;
}

EFirePlayState AFireEscapeGameMode::GetCurrentState() const
{
	return CurrentState;
}

void AFireEscapeGameMode::SetCurrentState(EFirePlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(NewState);
}

float AFireEscapeGameMode::GetFinalTimeDecimal2() const
{
	return FinalTimeDecimal2;
}

float AFireEscapeGameMode::GetFinalTimeDecimal1() const
{
	return FinalTimeDecimal1;
}

float AFireEscapeGameMode::GetFinalTimeSecond() const
{
	return FinalTimeSecond;
}

float AFireEscapeGameMode::GetFinalTimeMinute() const
{
	return FinalTimeMinute;
}

void AFireEscapeGameMode::HandleNewState(EFirePlayState NewState)
{
	switch (NewState)
	{
	case EFirePlayState::EPlaying: 
	{
	}
	break;

	//ゲームクリア時の時間を表示
	case EFirePlayState::EClear:
	{
		if (FinalTimeCheck == false)
		{
			AFireEscapeCharacter* MyCharacter = Cast<AFireEscapeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

			FinalTimeMinute = MyCharacter->GetTimeMinute();
			FinalTimeSecond = MyCharacter->GetTimeSecond();
			FinalTimeDecimal1 = MyCharacter->GetTimeDecimal1();
			FinalTimeDecimal2 = MyCharacter->GetTimeDecimal2();

			FinalTimeCheck = true;
		}
	}
	break;
	case EFirePlayState::EGameOver:
	{
		if (FinalTimeCheck == false)
		{
			AFireEscapeCharacter* MyCharacter = Cast<AFireEscapeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

			FinalTimeMinute = MyCharacter->GetTimeMinute();
			FinalTimeSecond = MyCharacter->GetTimeSecond();
			FinalTimeDecimal1 = MyCharacter->GetTimeDecimal1();
			FinalTimeDecimal2 = MyCharacter->GetTimeDecimal2();

			FinalTimeCheck = true;
		}
	}
	break;
	case EFirePlayState::EUnknown: break;
	}

}
