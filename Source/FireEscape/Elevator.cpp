// Fill out your copyright notice in the Description page of Project Settings.

#include "Elevator.h"


// Sets default values
AElevator::AElevator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AElevator::BeginPlay()
{
	Super::BeginPlay();
	
	// 初期速度と初期位置の設定をする
	velocity = 1.0f;                           // 追加
	startLocation = this->GetActorLocation();  // 追加
}

// Called every frame
void AElevator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// ここから追加
	FVector location = this->GetActorLocation();
	location.Z += velocity;
	if (location.Z < startLocation.Z || location.Z > startLocation.Z + 1000.0f) {
		velocity = 0;
	}
	this->SetActorLocation(location);
	// ここまで追加
	// 初期位置のZ+500まで上昇したら下降に転じて、0まで下がったらまた上昇させる
}

