// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Damage.generated.h"

UCLASS()
class FIREESCAPE_API ADamage : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADamage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// レベル上でダメージを与えるメッシュ
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Damage", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* DamageMesh;

public:
	// ダメージを与えるメッシュを返す
	FORCEINLINE UStaticMeshComponent* GetMesh() const { return DamageMesh; };

	// これがダメージを与えるかどうかの確認用
	UFUNCTION(BlueprintPure, Category = "Damage")
		bool IsActive();

	// ダメージを与えるかどうかを設定する
	UFUNCTION(BlueprintCallable, Category = "Damage")
		void SetActive(bool NewDamageState);
	/*
	// 範囲突入時に呼び出される
	UFUNCTION(BlueprintNativeEvent, Category = "Damage")
		void WasDamaged();
	virtual void WasDamaged_Implementation();*/

protected:
	// これがダメージを与えるかどうか
	bool bIsActive;
};
