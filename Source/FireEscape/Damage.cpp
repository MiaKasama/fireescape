// Fill out your copyright notice in the Description page of Project Settings.

#include "Damage.h"


// Sets default values
ADamage::ADamage()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// オブジェクトが生成された時点でアクティブにしておく
	bIsActive = true;

	// オブジェクトのメッシュを設定する
	DamageMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DamageMesh"));

	// StaticMeshコンポーネントをルートに設定
	RootComponent = DamageMesh;
}

// Called when the game starts or when spawned
void ADamage::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADamage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// bIsActiveのGetter
bool ADamage::IsActive()
{
	return bIsActive;
}

// bIsActiveのSetter
void ADamage::SetActive(bool NewDamageState)
{
	bIsActive = NewDamageState;
}
/*
void ADamage::WasDamaged_Implementation()
{
	//とりあえずログに吐き出す処理を追加
	FString name = GetName();
	UE_LOG(LogTemp, Log, TEXT("You have Damaged %s"), *name)
}*/

