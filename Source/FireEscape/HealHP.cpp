// Fill out your copyright notice in the Description page of Project Settings.

#include "HealHP.h"


// Sets default values
AHealHP::AHealHP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// オブジェクトが生成された時点でアクティブにしておく
	bHealActive = true;

	// オブジェクトのメッシュを設定する
	HealMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HealMesh"));

	// StaticMeshコンポーネントをルートに設定
	RootComponent = HealMesh;
}

// Called when the game starts or when spawned
void AHealHP::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHealHP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// bHealActiveのGetter
bool AHealHP::HealActive()
{
	return bHealActive;
}

// bHealActiveのSetter
void AHealHP::SetHealActive(bool NewHealState)
{
	bHealActive = NewHealState;
}

void AHealHP::WasHealed_Implementation()
{
	//とりあえずログに吐き出す処理を追加
	FString name = GetName();
	UE_LOG(LogTemp, Log, TEXT("You have Healed %s"), *name)
}
