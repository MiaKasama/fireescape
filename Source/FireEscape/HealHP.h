// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HealHP.generated.h"

UCLASS()
class FIREESCAPE_API AHealHP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHealHP();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// レベル上でHP回復をするメッシュ
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Heal", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* HealMesh;

public:
	// HP回復をするメッシュを返す
	FORCEINLINE UStaticMeshComponent* GetHealMesh() const { return HealMesh; };

	// これがHP回復するかどうかの確認用
	UFUNCTION(BlueprintPure, Category = "Heal")
		bool HealActive();

	// HP回復するかどうかを設定する
	UFUNCTION(BlueprintCallable, Category = "Heal")
		void SetHealActive(bool NewDamageState);

	// アイテム取得時に呼び出される
	UFUNCTION(BlueprintNativeEvent, Category = "Heal")
		void WasHealed();
	virtual void WasHealed_Implementation();

protected:
	// これがHP回復するかどうか
	bool bHealActive;
	
};
