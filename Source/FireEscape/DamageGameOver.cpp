// Fill out your copyright notice in the Description page of Project Settings.

#include "DamageGameOver.h"


// Sets default values
ADamageGameOver::ADamageGameOver()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// オブジェクトが生成された時点でアクティブにしておく
	bIsOverActive = true;

	// オブジェクトのメッシュを設定する
	GameOverMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GameOverMesh"));

	// StaticMeshコンポーネントをルートに設定
	RootComponent = GameOverMesh;
}

// Called when the game starts or when spawned
void ADamageGameOver::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADamageGameOver::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ADamageGameOver::IsOverActive()
{
	return bIsOverActive;
}

void ADamageGameOver::SetOverActive(bool NewOverState)
{
	bIsOverActive = NewOverState;
}

