// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FireEscapeCharacter.generated.h"

UCLASS(config=Game)
class AFireEscapeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AFireEscapeCharacter();

	// BeginPlayのオーバーライド
	virtual void BeginPlay() override;

	// Tickのオーバーライド
	virtual void Tick(float DeltaTime) override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	// ダメージを受ける範囲
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCapsuleComponent* ReceivingCapsule;

	// HPの値(リアルタイムに変化する方)
	UPROPERTY(VisibleAnywhere, Category = "Health")
		float CharacterHP;

	// 現在のX座標
	UPROPERTY(VisibleAnywhere, Category = "Position")
		float CharacterPositionX;

	// 現在のY座標
	UPROPERTY(VisibleAnywhere, Category = "Position")
		float CharacterPositionY;

	// 現在のZ座標
	UPROPERTY(VisibleAnywhere, Category = "Position")
		float CharacterPositionZ;

protected:
	// HP初期値(不変)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", meta = (BlueprintProtected = "true"))
		float InitialHP;
	
	// 時間経過(コンマ)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Time", meta = (BlueprintProtected = "true"))
		int TimeDecimal1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Time", meta = (BlueprintProtected = "true"))
		int TimeDecimal2;

	// 時間経過(秒)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Time", meta = (BlueprintProtected = "true"))
		int TimeSecond;

	// 時間経過(分)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Time", meta = (BlueprintProtected = "true"))
		int TimeMinute;

	// 時間加算
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Time", meta = (BlueprintProtected = "true"))
		int TimePlus;

	// 時間経過関数
	UFUNCTION(BlueprintCallable, Category = "Time")
		void Timer();
	/*
	// 回復中
	UFUNCTION(BlueprintCallable, Category = "Heal")
		void HealingTime();
	
	// 回復中のfor文用変数
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (BlueprintProtected = "true"))
		int iHeal;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (BlueprintProtected = "true"))
		int iHealCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (BlueprintProtected = "true"))
		int iHealedHP;*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Time", meta = (BlueprintProtected = "true"))
		UWorld* World;

	// アイテム収集ボタンが押された時に呼び出される。収集範囲内に入っていたHealHPを収集してHPを回復させる。
	UFUNCTION(BlueprintCallable, Category = "Heal")
		void CollectHeal();

	// アイテムを使って回復したかどうか（使用したならパーフェクトクリアとしない）
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (BlueprintProtected = "true"))
		bool boolHealed;

public:
	// 範囲内に突入したときにダメージが蓄積される
	UFUNCTION(BlueprintCallable, Category = "Damage")
		void DamageHP();

	// ダメージを喰らう範囲を返す
	FORCEINLINE class UCapsuleComponent* GetReceivingCapsule() const { return ReceivingCapsule; }

	// 範囲内に突入したら強制ゲームオーバー
	UFUNCTION(BlueprintCallable, Category = "GameOver")
		void GameOverHP();

	// HP初期値取得
	UFUNCTION(BlueprintPure, Category = "Health")
		float GetInitialHP();

	// HP取得
	UFUNCTION(BlueprintPure, Category = "Health")
		float GetCharacterHP();

	// HP更新
	UFUNCTION(BlueprintCallable, Category = "Health")
		void UpdateCharacterHP(float HPChange);

	// HPによってキャラクターメッシュの色を変化させる
	UFUNCTION(BlueprintImplementableEvent, Category = "Health")
		void HPChangeEffect();

	// 座標取得
	UFUNCTION(BlueprintPure, Category = "Position")
		float GetCharacterX();

	UFUNCTION(BlueprintPure, Category = "Position")
		float GetCharacterY();

	UFUNCTION(BlueprintPure, Category = "Position")
		float GetCharacterZ();

	// 経過時間取得
	UFUNCTION(BlueprintPure, Category = "Timer")
		float GetTimeDecimal1();

	UFUNCTION(BlueprintPure, Category = "Timer")
		float GetTimeDecimal2();

	UFUNCTION(BlueprintPure, Category = "Timer")
		float GetTimeSecond();

	UFUNCTION(BlueprintPure, Category = "Timer")
		float GetTimeMinute();

	// アイテム回収是非取得
	UFUNCTION(BlueprintPure, Category = "Heal")
		bool GetBoolHealed();
};

