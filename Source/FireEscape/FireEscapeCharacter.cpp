// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FireEscapeCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Damage.h"
//#include "FireDamage.h"
#include "DamageGameOver.h"
#include "HealHP.h"
#include "ItemHealHP.h"

//////////////////////////////////////////////////////////////////////////
// AFireEscapeCharacter

AFireEscapeCharacter::AFireEscapeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	// ReceivingCapsuleの設定
	ReceivingCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ReceivingCapsule"));
	
	// ReceivingCapsuleをルートの子に設定
	ReceivingCapsule->AttachTo(RootComponent);

	// Capsuleの半径と半高を設定
	ReceivingCapsule->SetCapsuleHalfHeight(100.0f);
	ReceivingCapsule->SetCapsuleRadius(50.0f);

	// HPデフォルト値設定
	InitialHP = 1.0f;
	CharacterHP = InitialHP;

}

void AFireEscapeCharacter::BeginPlay()
{
	Super::BeginPlay();

	boolHealed = false;

	//時間計測
	//初期値
	TimeDecimal1 = 0;
	TimeDecimal2 = 0;
	TimeSecond = 0;
	TimeMinute = 0;

	TimePlus = 1;

	//0.01秒毎にカウントを進める
	World = GEngine->GameViewport->GetWorld();
	FTimerHandle _TimerHandle;
	World->GetTimerManager().SetTimer(_TimerHandle, this, &AFireEscapeCharacter::Timer, 0.01f, true);
}

void AFireEscapeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// キャラクターの現在位置を取得
	FVector CharacterPosition = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();

	CharacterPositionX = CharacterPosition.X;
	CharacterPositionY = CharacterPosition.Y;
	CharacterPositionZ = CharacterPosition.Z;

	GameOverHP();
	DamageHP();
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFireEscapeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFireEscapeCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFireEscapeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFireEscapeCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFireEscapeCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AFireEscapeCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AFireEscapeCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFireEscapeCharacter::OnResetVR);

	// アイテム収集ボタンのバインディング
	PlayerInputComponent->BindAction("Healing", IE_Released, this, &AFireEscapeCharacter::CollectHeal);
}

void AFireEscapeCharacter::Timer()
{
	if ((TimeMinute == 9 && TimeSecond >= 59 && TimeDecimal1 >= 9 && TimeDecimal2 >= 9) || TimeMinute >= 10)
	{
		TimeMinute = 9;
		TimeSecond = 59;
		TimeDecimal1 = 9;
		TimeDecimal2 = 9;

		TimePlus = 0;
	}

	TimeDecimal2 += TimePlus;

	if (TimeDecimal2 >= 10)
	{
		TimeDecimal2 = 0;
		TimeDecimal1 += TimePlus;

		if (TimeDecimal1 >= 10)
		{
			TimeDecimal1 = 0;
			TimeSecond += TimePlus;

			if (TimeSecond >= 60)
			{
				TimeSecond = 0;
				TimeMinute += TimePlus;
			}
		}
	}
}

void AFireEscapeCharacter::CollectHeal()
{
	/*
	World = GEngine->GameViewport->GetWorld();
	FTimerHandle _TimerHandle;
	World->GetTimerManager().SetTimer(_TimerHandle, this, &AFireEscapeCharacter::HealingTime, 0.01f, true);*/

	
	// 範囲内に入っているオブジェクトを取得する
	TArray<AActor*> HealedActors;
	ReceivingCapsule->GetOverlappingActors(HealedActors);

	float HealedHP = 0;
	int HealCount = 0;

	for (int32 isReceived = 0; isReceived < HealedActors.Num(); isReceived++)
	{
		AHealHP* const TestHeal = Cast<AHealHP>(HealedActors[isReceived]);

		// キャストが成功し、破棄されておらず、アクティブな時だけ実行
		if (TestHeal && TestHeal->HealActive() && CharacterHP > 0)
		{
			TestHeal->WasHealed();

			AItemHealHP* const TestItem = Cast<AItemHealHP>(TestHeal);
			if (TestItem)
			{
				HealedHP += TestItem->GetItem();
				//HealedHP += 0.005f;
				//HealCount = (TestItem->GetItem()) / 0.005f;
			}

			TestHeal->SetHealActive(false);
		}
	}

	// 回復した分のHPを反映させる
	if (HealedHP > 0)
	{
		boolHealed = true;
		UpdateCharacterHP(HealedHP);
	}
}
/*
void AFireEscapeCharacter::HealingTime()
{
	
	//UE_LOG(LogTemp, Log, TEXT("iHeal = %d"), iHeal);

	// 範囲内に入っているオブジェクトを取得する
	TArray<AActor*> HealedActors;
	ReceivingCapsule->GetOverlappingActors(HealedActors);

	float HealedHP;
	int HealCount;

	if (iHeal == 0)
	{
		HealedHP = 0;
		HealCount = 0;
	}
	else
	{
		HealedHP = iHealedHP + 0.005f;
		iHealedHP += HealedHP;

		HealCount = iHealCount;

		iHeal++;
	}

	for (int32 isReceived = 0; isReceived < HealedActors.Num(); isReceived++)
	{
		AHealHP* const TestHeal = Cast<AHealHP>(HealedActors[isReceived]);

		// キャストが成功し、破棄されておらず、アクティブな時だけ実行
		if (TestHeal && TestHeal->HealActive() && CharacterHP > 0)
		{
			TestHeal->WasHealed();

			AItemHealHP* const TestItem = Cast<AItemHealHP>(TestHeal);
			if (TestItem)
			{
				//HealedHP += TestItem->GetItem();
				HealedHP += 0.005f;
				HealCount = (TestItem->GetItem()) / 0.005f;

				iHealCount = HealCount;
				iHealedHP = HealedHP;

				iHeal++;
			}

			TestHeal->SetHealActive(false);
		}
	}

	World = GEngine->GameViewport->GetWorld();
	FTimerHandle _TimerHandle;

	// 回復した分のHPを反映させる
	if (HealedHP > 0)
	{
		boolHealed = true;
		UpdateCharacterHP(HealedHP);

		UE_LOG(LogTemp, Log, TEXT("iHeal = %d"), iHeal);
		UE_LOG(LogTemp, Log, TEXT("HealedHP = %f"), HealedHP);
		UE_LOG(LogTemp, Log, TEXT("HealCount = %d"), HealCount);
		
		if (iHeal > HealCount)
		{
			UE_LOG(LogTemp, Log, TEXT("Stop"));
			World->GetTimerManager().ClearTimer(_TimerHandle);
		}
	}
}*/

void AFireEscapeCharacter::DamageHP()
{
	// 範囲内に入っているオブジェクトを取得する
	TArray<AActor*> DamageActors;
	ReceivingCapsule->GetOverlappingActors(DamageActors);

	float DamageFire = 0;
	
	for (int32 isReceived = 0; isReceived < DamageActors.Num(); isReceived++)
	{
		ADamage* const TestDamage = Cast<ADamage>(DamageActors[isReceived]);

		if (TestDamage && TestDamage->IsActive() && CharacterHP > 0)
		{
			DamageFire -= 0.005f;
		}
	}

	// ダメージを反映させる
	if (DamageFire < 0)
		UpdateCharacterHP(DamageFire);

}

void AFireEscapeCharacter::GameOverHP()
{
	// 範囲内に入っているオブジェクトを取得する
	TArray<AActor*> GameOverActors;
	ReceivingCapsule->GetOverlappingActors(GameOverActors);

	float GameOverFire = 0;

	for (int32 isReceived = 0; isReceived < GameOverActors.Num(); isReceived++)
	{
		ADamageGameOver* const TestGameOver = Cast<ADamageGameOver>(GameOverActors[isReceived]);

		if (TestGameOver && TestGameOver->IsOverActive() && CharacterHP > 0)
		{
			GameOverFire -= InitialHP;
		}
	}

	// ダメージを反映させる
	if (GameOverFire < 0)
		UpdateCharacterHP(GameOverFire);
}

float AFireEscapeCharacter::GetInitialHP()
{
	return InitialHP;
}

float AFireEscapeCharacter::GetCharacterHP()
{
	return CharacterHP;
}

void AFireEscapeCharacter::UpdateCharacterHP(float HPChange)
{
	// HPの更新
	CharacterHP = CharacterHP + HPChange;

	if (CharacterHP >= InitialHP)
		CharacterHP = InitialHP;

	//if (CharacterHP <= 0)
		//TimePlus = 0;

	// HPの値によって色を変える
	HPChangeEffect();
}

float AFireEscapeCharacter::GetCharacterX()
{
	return CharacterPositionX;
}

float AFireEscapeCharacter::GetCharacterY()
{
	return CharacterPositionY;
}

float AFireEscapeCharacter::GetCharacterZ()
{
	return CharacterPositionZ;
}

float AFireEscapeCharacter::GetTimeDecimal1()
{
	return TimeDecimal1;
}

float AFireEscapeCharacter::GetTimeDecimal2()
{
	return TimeDecimal2;
}

float AFireEscapeCharacter::GetTimeSecond()
{
	return TimeSecond;
}

float AFireEscapeCharacter::GetTimeMinute()
{
	return TimeMinute;
}

bool AFireEscapeCharacter::GetBoolHealed()
{
	return boolHealed;
}

void AFireEscapeCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFireEscapeCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AFireEscapeCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AFireEscapeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFireEscapeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFireEscapeCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AFireEscapeCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
