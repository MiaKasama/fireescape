// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Damage.h"
#include "FireDamage.generated.h"

/**
 * 
 */
UCLASS()
class FIREESCAPE_API AFireDamage : public ADamage
{
	GENERATED_BODY()

public:

	AFireDamage();

	FORCEINLINE float GetDive() const { return FireDive; }
	void SetDive(float Diving) { FireDive = Diving; }

	//void WasDamaged_Implementation() override;
	
protected:

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dive")
		float FireDive;

};
