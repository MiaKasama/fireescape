// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_FireEscapeCharacter_generated_h
#error "FireEscapeCharacter.generated.h already included, missing '#pragma once' in FireEscapeCharacter.h"
#endif
#define FIREESCAPE_FireEscapeCharacter_generated_h

#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBoolHealed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetBoolHealed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeMinute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeMinute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeSecond) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeSecond(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeDecimal2) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeDecimal2(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeDecimal1) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeDecimal1(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterZ) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterZ(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterY) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterY(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterX) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterX(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateCharacterHP) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_HPChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCharacterHP(Z_Param_HPChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGameOverHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GameOverHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDamageHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCollectHeal) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CollectHeal(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Timer(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBoolHealed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetBoolHealed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeMinute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeMinute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeSecond) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeSecond(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeDecimal2) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeDecimal2(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimeDecimal1) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetTimeDecimal1(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterZ) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterZ(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterY) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterY(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterX) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterX(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateCharacterHP) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_HPChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCharacterHP(Z_Param_HPChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCharacterHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCharacterHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGameOverHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GameOverHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDamageHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCollectHeal) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CollectHeal(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Timer(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_EVENT_PARMS
#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_CALLBACK_WRAPPERS
#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFireEscapeCharacter(); \
	friend struct Z_Construct_UClass_AFireEscapeCharacter_Statics; \
public: \
	DECLARE_CLASS(AFireEscapeCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AFireEscapeCharacter)


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFireEscapeCharacter(); \
	friend struct Z_Construct_UClass_AFireEscapeCharacter_Statics; \
public: \
	DECLARE_CLASS(AFireEscapeCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AFireEscapeCharacter)


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFireEscapeCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFireEscapeCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFireEscapeCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFireEscapeCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFireEscapeCharacter(AFireEscapeCharacter&&); \
	NO_API AFireEscapeCharacter(const AFireEscapeCharacter&); \
public:


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFireEscapeCharacter(AFireEscapeCharacter&&); \
	NO_API AFireEscapeCharacter(const AFireEscapeCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFireEscapeCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFireEscapeCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFireEscapeCharacter)


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AFireEscapeCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AFireEscapeCharacter, FollowCamera); } \
	FORCEINLINE static uint32 __PPO__ReceivingCapsule() { return STRUCT_OFFSET(AFireEscapeCharacter, ReceivingCapsule); } \
	FORCEINLINE static uint32 __PPO__CharacterHP() { return STRUCT_OFFSET(AFireEscapeCharacter, CharacterHP); } \
	FORCEINLINE static uint32 __PPO__CharacterPositionX() { return STRUCT_OFFSET(AFireEscapeCharacter, CharacterPositionX); } \
	FORCEINLINE static uint32 __PPO__CharacterPositionY() { return STRUCT_OFFSET(AFireEscapeCharacter, CharacterPositionY); } \
	FORCEINLINE static uint32 __PPO__CharacterPositionZ() { return STRUCT_OFFSET(AFireEscapeCharacter, CharacterPositionZ); } \
	FORCEINLINE static uint32 __PPO__InitialHP() { return STRUCT_OFFSET(AFireEscapeCharacter, InitialHP); } \
	FORCEINLINE static uint32 __PPO__TimeDecimal1() { return STRUCT_OFFSET(AFireEscapeCharacter, TimeDecimal1); } \
	FORCEINLINE static uint32 __PPO__TimeDecimal2() { return STRUCT_OFFSET(AFireEscapeCharacter, TimeDecimal2); } \
	FORCEINLINE static uint32 __PPO__TimeSecond() { return STRUCT_OFFSET(AFireEscapeCharacter, TimeSecond); } \
	FORCEINLINE static uint32 __PPO__TimeMinute() { return STRUCT_OFFSET(AFireEscapeCharacter, TimeMinute); } \
	FORCEINLINE static uint32 __PPO__TimePlus() { return STRUCT_OFFSET(AFireEscapeCharacter, TimePlus); } \
	FORCEINLINE static uint32 __PPO__World() { return STRUCT_OFFSET(AFireEscapeCharacter, World); } \
	FORCEINLINE static uint32 __PPO__boolHealed() { return STRUCT_OFFSET(AFireEscapeCharacter, boolHealed); }


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_9_PROLOG \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_EVENT_PARMS


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_CALLBACK_WRAPPERS \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_INCLASS \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_CALLBACK_WRAPPERS \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_FireEscapeCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_FireEscapeCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
