// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_ItemHealHP_generated_h
#error "ItemHealHP.generated.h already included, missing '#pragma once' in ItemHealHP.h"
#endif
#define FIREESCAPE_ItemHealHP_generated_h

#define FireEscape_Source_FireEscape_ItemHealHP_h_15_RPC_WRAPPERS
#define FireEscape_Source_FireEscape_ItemHealHP_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FireEscape_Source_FireEscape_ItemHealHP_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAItemHealHP(); \
	friend struct Z_Construct_UClass_AItemHealHP_Statics; \
public: \
	DECLARE_CLASS(AItemHealHP, AHealHP, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AItemHealHP)


#define FireEscape_Source_FireEscape_ItemHealHP_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAItemHealHP(); \
	friend struct Z_Construct_UClass_AItemHealHP_Statics; \
public: \
	DECLARE_CLASS(AItemHealHP, AHealHP, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AItemHealHP)


#define FireEscape_Source_FireEscape_ItemHealHP_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AItemHealHP(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AItemHealHP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItemHealHP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItemHealHP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItemHealHP(AItemHealHP&&); \
	NO_API AItemHealHP(const AItemHealHP&); \
public:


#define FireEscape_Source_FireEscape_ItemHealHP_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItemHealHP(AItemHealHP&&); \
	NO_API AItemHealHP(const AItemHealHP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItemHealHP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItemHealHP); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AItemHealHP)


#define FireEscape_Source_FireEscape_ItemHealHP_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HealItem() { return STRUCT_OFFSET(AItemHealHP, HealItem); }


#define FireEscape_Source_FireEscape_ItemHealHP_h_12_PROLOG
#define FireEscape_Source_FireEscape_ItemHealHP_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_INCLASS \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_ItemHealHP_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_ItemHealHP_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_ItemHealHP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
