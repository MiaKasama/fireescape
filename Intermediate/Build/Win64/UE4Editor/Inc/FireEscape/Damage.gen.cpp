// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FireEscape/Damage.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDamage() {}
// Cross Module References
	FIREESCAPE_API UClass* Z_Construct_UClass_ADamage_NoRegister();
	FIREESCAPE_API UClass* Z_Construct_UClass_ADamage();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FireEscape();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_ADamage_IsActive();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_ADamage_SetActive();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ADamage::StaticRegisterNativesADamage()
	{
		UClass* Class = ADamage::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsActive", &ADamage::execIsActive },
			{ "SetActive", &ADamage::execSetActive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADamage_IsActive_Statics
	{
		struct Damage_eventIsActive_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ADamage_IsActive_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Damage_eventIsActive_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ADamage_IsActive_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Damage_eventIsActive_Parms), &Z_Construct_UFunction_ADamage_IsActive_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADamage_IsActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADamage_IsActive_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADamage_IsActive_Statics::Function_MetaDataParams[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Damage.h" },
		{ "ToolTip", "???\xea\x82\xaa?_???[?W???^???\xe9\x82\xa9?\xc7\x82????\xcc\x8am?F?p" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADamage_IsActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADamage, "IsActive", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(Damage_eventIsActive_Parms), Z_Construct_UFunction_ADamage_IsActive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADamage_IsActive_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADamage_IsActive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADamage_IsActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADamage_IsActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADamage_IsActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADamage_SetActive_Statics
	{
		struct Damage_eventSetActive_Parms
		{
			bool NewDamageState;
		};
		static void NewProp_NewDamageState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_NewDamageState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ADamage_SetActive_Statics::NewProp_NewDamageState_SetBit(void* Obj)
	{
		((Damage_eventSetActive_Parms*)Obj)->NewDamageState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ADamage_SetActive_Statics::NewProp_NewDamageState = { UE4CodeGen_Private::EPropertyClass::Bool, "NewDamageState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Damage_eventSetActive_Parms), &Z_Construct_UFunction_ADamage_SetActive_Statics::NewProp_NewDamageState_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADamage_SetActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADamage_SetActive_Statics::NewProp_NewDamageState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADamage_SetActive_Statics::Function_MetaDataParams[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Damage.h" },
		{ "ToolTip", "?_???[?W???^???\xe9\x82\xa9?\xc7\x82??????\xdd\x92\xe8\x82\xb7??" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADamage_SetActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADamage, "SetActive", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Damage_eventSetActive_Parms), Z_Construct_UFunction_ADamage_SetActive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADamage_SetActive_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADamage_SetActive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADamage_SetActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADamage_SetActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADamage_SetActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADamage_NoRegister()
	{
		return ADamage::StaticClass();
	}
	struct Z_Construct_UClass_ADamage_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DamageMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADamage_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_FireEscape,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADamage_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ADamage_IsActive, "IsActive" }, // 405774358
		{ &Z_Construct_UFunction_ADamage_SetActive, "SetActive" }, // 2526596936
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADamage_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Damage.h" },
		{ "ModuleRelativePath", "Damage.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADamage_Statics::NewProp_DamageMesh_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Damage" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Damage.h" },
		{ "ToolTip", "???x?????\xc5\x83_???[?W???^???\xe9\x83\x81?b?V??" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADamage_Statics::NewProp_DamageMesh = { UE4CodeGen_Private::EPropertyClass::Object, "DamageMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(ADamage, DamageMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADamage_Statics::NewProp_DamageMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADamage_Statics::NewProp_DamageMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADamage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADamage_Statics::NewProp_DamageMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADamage_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADamage>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADamage_Statics::ClassParams = {
		&ADamage::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ADamage_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ADamage_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ADamage_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADamage_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADamage()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADamage_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADamage, 1203392202);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADamage(Z_Construct_UClass_ADamage, &ADamage::StaticClass, TEXT("/Script/FireEscape"), TEXT("ADamage"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADamage);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
