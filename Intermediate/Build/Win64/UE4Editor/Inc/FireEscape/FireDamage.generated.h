// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_FireDamage_generated_h
#error "FireDamage.generated.h already included, missing '#pragma once' in FireDamage.h"
#endif
#define FIREESCAPE_FireDamage_generated_h

#define FireEscape_Source_FireEscape_FireDamage_h_15_RPC_WRAPPERS
#define FireEscape_Source_FireEscape_FireDamage_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FireEscape_Source_FireEscape_FireDamage_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFireDamage(); \
	friend struct Z_Construct_UClass_AFireDamage_Statics; \
public: \
	DECLARE_CLASS(AFireDamage, ADamage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AFireDamage)


#define FireEscape_Source_FireEscape_FireDamage_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAFireDamage(); \
	friend struct Z_Construct_UClass_AFireDamage_Statics; \
public: \
	DECLARE_CLASS(AFireDamage, ADamage, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AFireDamage)


#define FireEscape_Source_FireEscape_FireDamage_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFireDamage(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFireDamage) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFireDamage); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFireDamage); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFireDamage(AFireDamage&&); \
	NO_API AFireDamage(const AFireDamage&); \
public:


#define FireEscape_Source_FireEscape_FireDamage_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFireDamage(AFireDamage&&); \
	NO_API AFireDamage(const AFireDamage&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFireDamage); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFireDamage); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFireDamage)


#define FireEscape_Source_FireEscape_FireDamage_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FireDive() { return STRUCT_OFFSET(AFireDamage, FireDive); }


#define FireEscape_Source_FireEscape_FireDamage_h_12_PROLOG
#define FireEscape_Source_FireEscape_FireDamage_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_FireDamage_h_15_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_FireDamage_h_15_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_FireDamage_h_15_INCLASS \
	FireEscape_Source_FireEscape_FireDamage_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_FireDamage_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_FireDamage_h_15_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_FireDamage_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_FireDamage_h_15_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_FireDamage_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_FireDamage_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
