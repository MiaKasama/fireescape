// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FireEscape/HealHP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHealHP() {}
// Cross Module References
	FIREESCAPE_API UClass* Z_Construct_UClass_AHealHP_NoRegister();
	FIREESCAPE_API UClass* Z_Construct_UClass_AHealHP();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FireEscape();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AHealHP_HealActive();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AHealHP_SetHealActive();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AHealHP_WasHealed();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AHealHP_WasHealed = FName(TEXT("WasHealed"));
	void AHealHP::WasHealed()
	{
		ProcessEvent(FindFunctionChecked(NAME_AHealHP_WasHealed),NULL);
	}
	void AHealHP::StaticRegisterNativesAHealHP()
	{
		UClass* Class = AHealHP::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "HealActive", &AHealHP::execHealActive },
			{ "SetHealActive", &AHealHP::execSetHealActive },
			{ "WasHealed", &AHealHP::execWasHealed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AHealHP_HealActive_Statics
	{
		struct HealHP_eventHealActive_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AHealHP_HealActive_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((HealHP_eventHealActive_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AHealHP_HealActive_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(HealHP_eventHealActive_Parms), &Z_Construct_UFunction_AHealHP_HealActive_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AHealHP_HealActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AHealHP_HealActive_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AHealHP_HealActive_Statics::Function_MetaDataParams[] = {
		{ "Category", "Heal" },
		{ "ModuleRelativePath", "HealHP.h" },
		{ "ToolTip", "???\xea\x82\xaaHP?\xf1\x95\x9c\x82??\xe9\x82\xa9?\xc7\x82????\xcc\x8am?F?p" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AHealHP_HealActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AHealHP, "HealActive", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(HealHP_eventHealActive_Parms), Z_Construct_UFunction_AHealHP_HealActive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AHealHP_HealActive_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AHealHP_HealActive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AHealHP_HealActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AHealHP_HealActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AHealHP_HealActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AHealHP_SetHealActive_Statics
	{
		struct HealHP_eventSetHealActive_Parms
		{
			bool NewDamageState;
		};
		static void NewProp_NewDamageState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_NewDamageState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AHealHP_SetHealActive_Statics::NewProp_NewDamageState_SetBit(void* Obj)
	{
		((HealHP_eventSetHealActive_Parms*)Obj)->NewDamageState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AHealHP_SetHealActive_Statics::NewProp_NewDamageState = { UE4CodeGen_Private::EPropertyClass::Bool, "NewDamageState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(HealHP_eventSetHealActive_Parms), &Z_Construct_UFunction_AHealHP_SetHealActive_Statics::NewProp_NewDamageState_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AHealHP_SetHealActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AHealHP_SetHealActive_Statics::NewProp_NewDamageState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AHealHP_SetHealActive_Statics::Function_MetaDataParams[] = {
		{ "Category", "Heal" },
		{ "ModuleRelativePath", "HealHP.h" },
		{ "ToolTip", "HP?\xf1\x95\x9c\x82??\xe9\x82\xa9?\xc7\x82??????\xdd\x92\xe8\x82\xb7??" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AHealHP_SetHealActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AHealHP, "SetHealActive", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(HealHP_eventSetHealActive_Parms), Z_Construct_UFunction_AHealHP_SetHealActive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AHealHP_SetHealActive_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AHealHP_SetHealActive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AHealHP_SetHealActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AHealHP_SetHealActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AHealHP_SetHealActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AHealHP_WasHealed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AHealHP_WasHealed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Heal" },
		{ "ModuleRelativePath", "HealHP.h" },
		{ "ToolTip", "?A?C?e???\xe6\x93\xbe???\xc9\x8c\xc4\x82\xd1\x8fo??????" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AHealHP_WasHealed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AHealHP, "WasHealed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AHealHP_WasHealed_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AHealHP_WasHealed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AHealHP_WasHealed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AHealHP_WasHealed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AHealHP_NoRegister()
	{
		return AHealHP::StaticClass();
	}
	struct Z_Construct_UClass_AHealHP_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HealMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHealHP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_FireEscape,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AHealHP_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AHealHP_HealActive, "HealActive" }, // 2317521356
		{ &Z_Construct_UFunction_AHealHP_SetHealActive, "SetHealActive" }, // 4290787811
		{ &Z_Construct_UFunction_AHealHP_WasHealed, "WasHealed" }, // 759179846
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHealHP_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HealHP.h" },
		{ "ModuleRelativePath", "HealHP.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHealHP_Statics::NewProp_HealMesh_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Heal" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "HealHP.h" },
		{ "ToolTip", "???x??????HP?\xf1\x95\x9c\x82????\xe9\x83\x81?b?V??" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHealHP_Statics::NewProp_HealMesh = { UE4CodeGen_Private::EPropertyClass::Object, "HealMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AHealHP, HealMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHealHP_Statics::NewProp_HealMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHealHP_Statics::NewProp_HealMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AHealHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHealHP_Statics::NewProp_HealMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHealHP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHealHP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AHealHP_Statics::ClassParams = {
		&AHealHP::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AHealHP_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AHealHP_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AHealHP_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AHealHP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHealHP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AHealHP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHealHP, 2222501802);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHealHP(Z_Construct_UClass_AHealHP, &AHealHP::StaticClass, TEXT("/Script/FireEscape"), TEXT("AHealHP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHealHP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
