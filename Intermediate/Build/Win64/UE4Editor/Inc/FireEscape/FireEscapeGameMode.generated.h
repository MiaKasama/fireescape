// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EFirePlayState : uint8;
#ifdef FIREESCAPE_FireEscapeGameMode_generated_h
#error "FireEscapeGameMode.generated.h already included, missing '#pragma once' in FireEscapeGameMode.h"
#endif
#define FIREESCAPE_FireEscapeGameMode_generated_h

#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetFinalTimeMinute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeMinute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFinalTimeSecond) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeSecond(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFinalTimeDecimal1) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeDecimal1(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFinalTimeDecimal2) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeDecimal2(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EFirePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHPToOver) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetHPToOver(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetFinalTimeMinute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeMinute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFinalTimeSecond) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeSecond(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFinalTimeDecimal1) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeDecimal1(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFinalTimeDecimal2) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetFinalTimeDecimal2(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EFirePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHPToOver) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetHPToOver(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFireEscapeGameMode(); \
	friend struct Z_Construct_UClass_AFireEscapeGameMode_Statics; \
public: \
	DECLARE_CLASS(AFireEscapeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/FireEscape"), FIREESCAPE_API) \
	DECLARE_SERIALIZER(AFireEscapeGameMode)


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAFireEscapeGameMode(); \
	friend struct Z_Construct_UClass_AFireEscapeGameMode_Statics; \
public: \
	DECLARE_CLASS(AFireEscapeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/FireEscape"), FIREESCAPE_API) \
	DECLARE_SERIALIZER(AFireEscapeGameMode)


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FIREESCAPE_API AFireEscapeGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFireEscapeGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIREESCAPE_API, AFireEscapeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFireEscapeGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIREESCAPE_API AFireEscapeGameMode(AFireEscapeGameMode&&); \
	FIREESCAPE_API AFireEscapeGameMode(const AFireEscapeGameMode&); \
public:


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIREESCAPE_API AFireEscapeGameMode(AFireEscapeGameMode&&); \
	FIREESCAPE_API AFireEscapeGameMode(const AFireEscapeGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIREESCAPE_API, AFireEscapeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFireEscapeGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFireEscapeGameMode)


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FireRate() { return STRUCT_OFFSET(AFireEscapeGameMode, FireRate); } \
	FORCEINLINE static uint32 __PPO__HPToOver() { return STRUCT_OFFSET(AFireEscapeGameMode, HPToOver); } \
	FORCEINLINE static uint32 __PPO__ClearX() { return STRUCT_OFFSET(AFireEscapeGameMode, ClearX); } \
	FORCEINLINE static uint32 __PPO__ClearY() { return STRUCT_OFFSET(AFireEscapeGameMode, ClearY); } \
	FORCEINLINE static uint32 __PPO__ClearZ() { return STRUCT_OFFSET(AFireEscapeGameMode, ClearZ); } \
	FORCEINLINE static uint32 __PPO__HUDWidgetClass() { return STRUCT_OFFSET(AFireEscapeGameMode, HUDWidgetClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AFireEscapeGameMode, CurrentWidget); } \
	FORCEINLINE static uint32 __PPO__FinalTimeDecimal2() { return STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeDecimal2); } \
	FORCEINLINE static uint32 __PPO__FinalTimeDecimal1() { return STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeDecimal1); } \
	FORCEINLINE static uint32 __PPO__FinalTimeSecond() { return STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeSecond); } \
	FORCEINLINE static uint32 __PPO__FinalTimeMinute() { return STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeMinute); } \
	FORCEINLINE static uint32 __PPO__FinalTimeCheck() { return STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeCheck); }


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_19_PROLOG
#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_INCLASS \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_FireEscapeGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_FireEscapeGameMode_h


#define FOREACH_ENUM_EFIREPLAYSTATE(op) \
	op(EFirePlayState::EPlaying) \
	op(EFirePlayState::EGameOver) \
	op(EFirePlayState::EClear) \
	op(EFirePlayState::EUnknown) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
