// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_Damage_generated_h
#error "Damage.generated.h already included, missing '#pragma once' in Damage.h"
#endif
#define FIREESCAPE_Damage_generated_h

#define FireEscape_Source_FireEscape_Damage_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetActive) \
	{ \
		P_GET_UBOOL(Z_Param_NewDamageState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetActive(Z_Param_NewDamageState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsActive(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_Damage_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetActive) \
	{ \
		P_GET_UBOOL(Z_Param_NewDamageState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetActive(Z_Param_NewDamageState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsActive(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_Damage_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADamage(); \
	friend struct Z_Construct_UClass_ADamage_Statics; \
public: \
	DECLARE_CLASS(ADamage, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(ADamage)


#define FireEscape_Source_FireEscape_Damage_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADamage(); \
	friend struct Z_Construct_UClass_ADamage_Statics; \
public: \
	DECLARE_CLASS(ADamage, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(ADamage)


#define FireEscape_Source_FireEscape_Damage_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADamage(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADamage) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamage); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamage); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamage(ADamage&&); \
	NO_API ADamage(const ADamage&); \
public:


#define FireEscape_Source_FireEscape_Damage_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamage(ADamage&&); \
	NO_API ADamage(const ADamage&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamage); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamage); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADamage)


#define FireEscape_Source_FireEscape_Damage_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DamageMesh() { return STRUCT_OFFSET(ADamage, DamageMesh); }


#define FireEscape_Source_FireEscape_Damage_h_9_PROLOG
#define FireEscape_Source_FireEscape_Damage_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_Damage_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_Damage_h_12_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_Damage_h_12_INCLASS \
	FireEscape_Source_FireEscape_Damage_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_Damage_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_Damage_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_Damage_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_Damage_h_12_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_Damage_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_Damage_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
