// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_Elevator_generated_h
#error "Elevator.generated.h already included, missing '#pragma once' in Elevator.h"
#endif
#define FIREESCAPE_Elevator_generated_h

#define FireEscape_Source_FireEscape_Elevator_h_12_RPC_WRAPPERS
#define FireEscape_Source_FireEscape_Elevator_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FireEscape_Source_FireEscape_Elevator_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAElevator(); \
	friend struct Z_Construct_UClass_AElevator_Statics; \
public: \
	DECLARE_CLASS(AElevator, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AElevator)


#define FireEscape_Source_FireEscape_Elevator_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAElevator(); \
	friend struct Z_Construct_UClass_AElevator_Statics; \
public: \
	DECLARE_CLASS(AElevator, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AElevator)


#define FireEscape_Source_FireEscape_Elevator_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AElevator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AElevator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AElevator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AElevator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AElevator(AElevator&&); \
	NO_API AElevator(const AElevator&); \
public:


#define FireEscape_Source_FireEscape_Elevator_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AElevator(AElevator&&); \
	NO_API AElevator(const AElevator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AElevator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AElevator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AElevator)


#define FireEscape_Source_FireEscape_Elevator_h_12_PRIVATE_PROPERTY_OFFSET
#define FireEscape_Source_FireEscape_Elevator_h_9_PROLOG
#define FireEscape_Source_FireEscape_Elevator_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_Elevator_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_Elevator_h_12_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_Elevator_h_12_INCLASS \
	FireEscape_Source_FireEscape_Elevator_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_Elevator_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_Elevator_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_Elevator_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_Elevator_h_12_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_Elevator_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_Elevator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
