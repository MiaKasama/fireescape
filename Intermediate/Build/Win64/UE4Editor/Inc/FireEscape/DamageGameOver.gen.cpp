// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FireEscape/DamageGameOver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDamageGameOver() {}
// Cross Module References
	FIREESCAPE_API UClass* Z_Construct_UClass_ADamageGameOver_NoRegister();
	FIREESCAPE_API UClass* Z_Construct_UClass_ADamageGameOver();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FireEscape();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_ADamageGameOver_IsOverActive();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_ADamageGameOver_SetOverActive();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ADamageGameOver::StaticRegisterNativesADamageGameOver()
	{
		UClass* Class = ADamageGameOver::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsOverActive", &ADamageGameOver::execIsOverActive },
			{ "SetOverActive", &ADamageGameOver::execSetOverActive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics
	{
		struct DamageGameOver_eventIsOverActive_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DamageGameOver_eventIsOverActive_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(DamageGameOver_eventIsOverActive_Parms), &Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::Function_MetaDataParams[] = {
		{ "Category", "GameOver" },
		{ "ModuleRelativePath", "DamageGameOver.h" },
		{ "ToolTip", "???\xea\x82\xaa?Q?[???I?[?o?[?\xc9\x82??\xe9\x82\xa9?\xc7\x82????\xcc\x8am?F?p" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADamageGameOver, "IsOverActive", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(DamageGameOver_eventIsOverActive_Parms), Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADamageGameOver_IsOverActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADamageGameOver_IsOverActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics
	{
		struct DamageGameOver_eventSetOverActive_Parms
		{
			bool NewOverState;
		};
		static void NewProp_NewOverState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_NewOverState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::NewProp_NewOverState_SetBit(void* Obj)
	{
		((DamageGameOver_eventSetOverActive_Parms*)Obj)->NewOverState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::NewProp_NewOverState = { UE4CodeGen_Private::EPropertyClass::Bool, "NewOverState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(DamageGameOver_eventSetOverActive_Parms), &Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::NewProp_NewOverState_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::NewProp_NewOverState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::Function_MetaDataParams[] = {
		{ "Category", "GameOver" },
		{ "ModuleRelativePath", "DamageGameOver.h" },
		{ "ToolTip", "?Q?[???I?[?o?[?\xc9\x82??\xe9\x82\xa9?\xc7\x82??????\xdd\x92\xe8\x82\xb7??" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADamageGameOver, "SetOverActive", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(DamageGameOver_eventSetOverActive_Parms), Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADamageGameOver_SetOverActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADamageGameOver_SetOverActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADamageGameOver_NoRegister()
	{
		return ADamageGameOver::StaticClass();
	}
	struct Z_Construct_UClass_ADamageGameOver_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameOverMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameOverMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADamageGameOver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_FireEscape,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADamageGameOver_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ADamageGameOver_IsOverActive, "IsOverActive" }, // 737136648
		{ &Z_Construct_UFunction_ADamageGameOver_SetOverActive, "SetOverActive" }, // 1589212049
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADamageGameOver_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DamageGameOver.h" },
		{ "ModuleRelativePath", "DamageGameOver.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADamageGameOver_Statics::NewProp_GameOverMesh_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "GameOver" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "DamageGameOver.h" },
		{ "ToolTip", "???x?????\xc5\x8b????Q?[???I?[?o?[?\xc9\x82??\xe9\x83\x81?b?V??" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADamageGameOver_Statics::NewProp_GameOverMesh = { UE4CodeGen_Private::EPropertyClass::Object, "GameOverMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(ADamageGameOver, GameOverMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADamageGameOver_Statics::NewProp_GameOverMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADamageGameOver_Statics::NewProp_GameOverMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADamageGameOver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADamageGameOver_Statics::NewProp_GameOverMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADamageGameOver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADamageGameOver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADamageGameOver_Statics::ClassParams = {
		&ADamageGameOver::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ADamageGameOver_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ADamageGameOver_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ADamageGameOver_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADamageGameOver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADamageGameOver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADamageGameOver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADamageGameOver, 2854712770);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADamageGameOver(Z_Construct_UClass_ADamageGameOver, &ADamageGameOver::StaticClass, TEXT("/Script/FireEscape"), TEXT("ADamageGameOver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADamageGameOver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
