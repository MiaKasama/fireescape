// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_DamageGameOver_generated_h
#error "DamageGameOver.generated.h already included, missing '#pragma once' in DamageGameOver.h"
#endif
#define FIREESCAPE_DamageGameOver_generated_h

#define FireEscape_Source_FireEscape_DamageGameOver_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetOverActive) \
	{ \
		P_GET_UBOOL(Z_Param_NewOverState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetOverActive(Z_Param_NewOverState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsOverActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsOverActive(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetOverActive) \
	{ \
		P_GET_UBOOL(Z_Param_NewOverState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetOverActive(Z_Param_NewOverState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsOverActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsOverActive(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADamageGameOver(); \
	friend struct Z_Construct_UClass_ADamageGameOver_Statics; \
public: \
	DECLARE_CLASS(ADamageGameOver, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(ADamageGameOver)


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADamageGameOver(); \
	friend struct Z_Construct_UClass_ADamageGameOver_Statics; \
public: \
	DECLARE_CLASS(ADamageGameOver, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(ADamageGameOver)


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADamageGameOver(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADamageGameOver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamageGameOver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamageGameOver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamageGameOver(ADamageGameOver&&); \
	NO_API ADamageGameOver(const ADamageGameOver&); \
public:


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamageGameOver(ADamageGameOver&&); \
	NO_API ADamageGameOver(const ADamageGameOver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamageGameOver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamageGameOver); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADamageGameOver)


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GameOverMesh() { return STRUCT_OFFSET(ADamageGameOver, GameOverMesh); }


#define FireEscape_Source_FireEscape_DamageGameOver_h_9_PROLOG
#define FireEscape_Source_FireEscape_DamageGameOver_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_INCLASS \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_DamageGameOver_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_DamageGameOver_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_DamageGameOver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
