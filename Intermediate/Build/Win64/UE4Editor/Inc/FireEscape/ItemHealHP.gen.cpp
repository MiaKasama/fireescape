// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FireEscape/ItemHealHP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeItemHealHP() {}
// Cross Module References
	FIREESCAPE_API UClass* Z_Construct_UClass_AItemHealHP_NoRegister();
	FIREESCAPE_API UClass* Z_Construct_UClass_AItemHealHP();
	FIREESCAPE_API UClass* Z_Construct_UClass_AHealHP();
	UPackage* Z_Construct_UPackage__Script_FireEscape();
// End Cross Module References
	void AItemHealHP::StaticRegisterNativesAItemHealHP()
	{
	}
	UClass* Z_Construct_UClass_AItemHealHP_NoRegister()
	{
		return AItemHealHP::StaticClass();
	}
	struct Z_Construct_UClass_AItemHealHP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealItem_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealItem;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AItemHealHP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHealHP,
		(UObject* (*)())Z_Construct_UPackage__Script_FireEscape,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AItemHealHP_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ItemHealHP.h" },
		{ "ModuleRelativePath", "ItemHealHP.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AItemHealHP_Statics::NewProp_HealItem_MetaData[] = {
		{ "Category", "HealItem" },
		{ "ModuleRelativePath", "ItemHealHP.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AItemHealHP_Statics::NewProp_HealItem = { UE4CodeGen_Private::EPropertyClass::Float, "HealItem", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000000005, 1, nullptr, STRUCT_OFFSET(AItemHealHP, HealItem), METADATA_PARAMS(Z_Construct_UClass_AItemHealHP_Statics::NewProp_HealItem_MetaData, ARRAY_COUNT(Z_Construct_UClass_AItemHealHP_Statics::NewProp_HealItem_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AItemHealHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AItemHealHP_Statics::NewProp_HealItem,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AItemHealHP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AItemHealHP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AItemHealHP_Statics::ClassParams = {
		&AItemHealHP::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AItemHealHP_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AItemHealHP_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AItemHealHP_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AItemHealHP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AItemHealHP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AItemHealHP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AItemHealHP, 2127215928);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AItemHealHP(Z_Construct_UClass_AItemHealHP, &AItemHealHP::StaticClass, TEXT("/Script/FireEscape"), TEXT("AItemHealHP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AItemHealHP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
