// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FireEscape/FireEscapeCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFireEscapeCharacter() {}
// Cross Module References
	FIREESCAPE_API UClass* Z_Construct_UClass_AFireEscapeCharacter_NoRegister();
	FIREESCAPE_API UClass* Z_Construct_UClass_AFireEscapeCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_FireEscape();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_DamageHP();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_Timer();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCapsuleComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	static FName NAME_AFireEscapeCharacter_HPChangeEffect = FName(TEXT("HPChangeEffect"));
	void AFireEscapeCharacter::HPChangeEffect()
	{
		ProcessEvent(FindFunctionChecked(NAME_AFireEscapeCharacter_HPChangeEffect),NULL);
	}
	void AFireEscapeCharacter::StaticRegisterNativesAFireEscapeCharacter()
	{
		UClass* Class = AFireEscapeCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CollectHeal", &AFireEscapeCharacter::execCollectHeal },
			{ "DamageHP", &AFireEscapeCharacter::execDamageHP },
			{ "GameOverHP", &AFireEscapeCharacter::execGameOverHP },
			{ "GetBoolHealed", &AFireEscapeCharacter::execGetBoolHealed },
			{ "GetCharacterHP", &AFireEscapeCharacter::execGetCharacterHP },
			{ "GetCharacterX", &AFireEscapeCharacter::execGetCharacterX },
			{ "GetCharacterY", &AFireEscapeCharacter::execGetCharacterY },
			{ "GetCharacterZ", &AFireEscapeCharacter::execGetCharacterZ },
			{ "GetInitialHP", &AFireEscapeCharacter::execGetInitialHP },
			{ "GetTimeDecimal1", &AFireEscapeCharacter::execGetTimeDecimal1 },
			{ "GetTimeDecimal2", &AFireEscapeCharacter::execGetTimeDecimal2 },
			{ "GetTimeMinute", &AFireEscapeCharacter::execGetTimeMinute },
			{ "GetTimeSecond", &AFireEscapeCharacter::execGetTimeSecond },
			{ "Timer", &AFireEscapeCharacter::execTimer },
			{ "UpdateCharacterHP", &AFireEscapeCharacter::execUpdateCharacterHP },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal_Statics::Function_MetaDataParams[] = {
		{ "Category", "Heal" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?A?C?e?????W?{?^?????????\xea\x82\xbd???\xc9\x8c\xc4\x82\xd1\x8fo???????B???W?\xcd\x88\xcd\x93??\xc9\x93????\xc4\x82???HealHP?????W????HP???\xf1\x95\x9c\x82??????B" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "CollectHeal", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04080401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_DamageHP_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_DamageHP_Statics::Function_MetaDataParams[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?\xcd\x88\xcd\x93??\xc9\x93\xcb\x93?\xc6\x82??\xc9\x83_???[?W???~?\xcf\x82?????" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_DamageHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "DamageHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_DamageHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_DamageHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_DamageHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_DamageHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP_Statics::Function_MetaDataParams[] = {
		{ "Category", "GameOver" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?\xcd\x88\xcd\x93??\xc9\x93\xcb\x93?\xe7\x8b\xad???Q?[???I?[?o?[" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GameOverHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics
	{
		struct FireEscapeCharacter_eventGetBoolHealed_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((FireEscapeCharacter_eventGetBoolHealed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FireEscapeCharacter_eventGetBoolHealed_Parms), &Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Heal" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?A?C?e???????????\xe6\x93\xbe" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetBoolHealed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetBoolHealed_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics
	{
		struct FireEscapeCharacter_eventGetCharacterHP_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetCharacterHP_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "HP?\xe6\x93\xbe" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetCharacterHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetCharacterHP_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics
	{
		struct FireEscapeCharacter_eventGetCharacterX_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetCharacterX_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::Function_MetaDataParams[] = {
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???W?\xe6\x93\xbe" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetCharacterX", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetCharacterX_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics
	{
		struct FireEscapeCharacter_eventGetCharacterY_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetCharacterY_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::Function_MetaDataParams[] = {
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetCharacterY", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetCharacterY_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics
	{
		struct FireEscapeCharacter_eventGetCharacterZ_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetCharacterZ_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::Function_MetaDataParams[] = {
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetCharacterZ", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetCharacterZ_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics
	{
		struct FireEscapeCharacter_eventGetInitialHP_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetInitialHP_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "HP?????l?\xe6\x93\xbe" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetInitialHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetInitialHP_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics
	{
		struct FireEscapeCharacter_eventGetTimeDecimal1_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetTimeDecimal1_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::Function_MetaDataParams[] = {
		{ "Category", "Timer" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?o?\xdf\x8e??\xd4\x8e\xe6\x93\xbe" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetTimeDecimal1", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetTimeDecimal1_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics
	{
		struct FireEscapeCharacter_eventGetTimeDecimal2_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetTimeDecimal2_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::Function_MetaDataParams[] = {
		{ "Category", "Timer" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetTimeDecimal2", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetTimeDecimal2_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics
	{
		struct FireEscapeCharacter_eventGetTimeMinute_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetTimeMinute_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::Function_MetaDataParams[] = {
		{ "Category", "Timer" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetTimeMinute", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetTimeMinute_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics
	{
		struct FireEscapeCharacter_eventGetTimeSecond_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventGetTimeSecond_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::Function_MetaDataParams[] = {
		{ "Category", "Timer" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "GetTimeSecond", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(FireEscapeCharacter_eventGetTimeSecond_Parms), Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "HP?\xc9\x82????\xc4\x83L?????N?^?[???b?V???\xcc\x90""F???\xcf\x89???????" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "HPChangeEffect", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_Timer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_Timer_Statics::Function_MetaDataParams[] = {
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xd4\x8co?\xdf\x8a\xd6\x90?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_Timer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "Timer", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04080401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_Timer_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_Timer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_Timer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_Timer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics
	{
		struct FireEscapeCharacter_eventUpdateCharacterHP_Parms
		{
			float HPChange;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HPChange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::NewProp_HPChange = { UE4CodeGen_Private::EPropertyClass::Float, "HPChange", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(FireEscapeCharacter_eventUpdateCharacterHP_Parms, HPChange), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::NewProp_HPChange,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "HP?X?V" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeCharacter, "UpdateCharacterHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(FireEscapeCharacter_eventUpdateCharacterHP_Parms), Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AFireEscapeCharacter_NoRegister()
	{
		return AFireEscapeCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AFireEscapeCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boolHealed_MetaData[];
#endif
		static void NewProp_boolHealed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_boolHealed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_World_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimePlus_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimePlus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeMinute_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimeMinute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeSecond_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimeSecond;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeDecimal2_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimeDecimal2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeDecimal1_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimeDecimal1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialHP_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InitialHP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterPositionZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterPositionZ;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterPositionY_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterPositionY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterPositionX_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterPositionX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterHP_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CharacterHP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReceivingCapsule_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReceivingCapsule;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFireEscapeCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_FireEscape,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AFireEscapeCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AFireEscapeCharacter_CollectHeal, "CollectHeal" }, // 266495079
		{ &Z_Construct_UFunction_AFireEscapeCharacter_DamageHP, "DamageHP" }, // 761156873
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GameOverHP, "GameOverHP" }, // 2193322052
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetBoolHealed, "GetBoolHealed" }, // 4219184182
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterHP, "GetCharacterHP" }, // 3092015594
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterX, "GetCharacterX" }, // 1077268409
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterY, "GetCharacterY" }, // 1985418495
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetCharacterZ, "GetCharacterZ" }, // 4212575342
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetInitialHP, "GetInitialHP" }, // 2697588063
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal1, "GetTimeDecimal1" }, // 1840555518
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetTimeDecimal2, "GetTimeDecimal2" }, // 2153433341
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetTimeMinute, "GetTimeMinute" }, // 2231486835
		{ &Z_Construct_UFunction_AFireEscapeCharacter_GetTimeSecond, "GetTimeSecond" }, // 3891250605
		{ &Z_Construct_UFunction_AFireEscapeCharacter_HPChangeEffect, "HPChangeEffect" }, // 2126955620
		{ &Z_Construct_UFunction_AFireEscapeCharacter_Timer, "Timer" }, // 130455328
		{ &Z_Construct_UFunction_AFireEscapeCharacter_UpdateCharacterHP, "UpdateCharacterHP" }, // 3275809989
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "FireEscapeCharacter.h" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Heal" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?A?C?e?????g???\xc4\x89\xf1\x95\x9c\x82??????\xc7\x82????i?g?p?????\xc8\x82??p?[?t?F?N?g?N???A?\xc6\x82??\xc8\x82??j" },
	};
#endif
	void Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed_SetBit(void* Obj)
	{
		((AFireEscapeCharacter*)Obj)->boolHealed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed = { UE4CodeGen_Private::EPropertyClass::Bool, "boolHealed", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AFireEscapeCharacter), &Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed_SetBit, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_World_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "// ?\xf1\x95\x9c\x92?\nUFUNCTION(BlueprintCallable, Category = \"Heal\")\n        void HealingTime();\n\n// ?\xf1\x95\x9c\x92???for???p?\xcf\x90?\nUPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = \"Heal\", meta = (BlueprintProtected = \"true\"))\n        int iHeal;\n\nUPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = \"Heal\", meta = (BlueprintProtected = \"true\"))\n        int iHealCount;\n\nUPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = \"Heal\", meta = (BlueprintProtected = \"true\"))\n        int iHealedHP;" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_World = { UE4CodeGen_Private::EPropertyClass::Object, "World", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_World_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_World_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimePlus_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xd4\x89??Z" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimePlus = { UE4CodeGen_Private::EPropertyClass::Int, "TimePlus", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, TimePlus), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimePlus_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimePlus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeMinute_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xd4\x8co??(??)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeMinute = { UE4CodeGen_Private::EPropertyClass::Int, "TimeMinute", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, TimeMinute), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeMinute_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeMinute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeSecond_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xd4\x8co??(?b)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeSecond = { UE4CodeGen_Private::EPropertyClass::Int, "TimeSecond", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, TimeSecond), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeSecond_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeSecond_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal2_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal2 = { UE4CodeGen_Private::EPropertyClass::Int, "TimeDecimal2", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, TimeDecimal2), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal2_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal1_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Time" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xd4\x8co??(?R???})" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal1 = { UE4CodeGen_Private::EPropertyClass::Int, "TimeDecimal1", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, TimeDecimal1), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal1_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_InitialHP_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "HP?????l(?s??)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_InitialHP = { UE4CodeGen_Private::EPropertyClass::Float, "InitialHP", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000000005, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, InitialHP), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_InitialHP_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_InitialHP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionZ_MetaData[] = {
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xdd\x82?Z???W" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionZ = { UE4CodeGen_Private::EPropertyClass::Float, "CharacterPositionZ", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000020001, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, CharacterPositionZ), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionZ_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionZ_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionY_MetaData[] = {
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xdd\x82?Y???W" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionY = { UE4CodeGen_Private::EPropertyClass::Float, "CharacterPositionY", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000020001, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, CharacterPositionY), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionY_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionX_MetaData[] = {
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "???\xdd\x82?X???W" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionX = { UE4CodeGen_Private::EPropertyClass::Float, "CharacterPositionX", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000020001, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, CharacterPositionX), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterHP_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "HP?\xcc\x92l(???A???^?C???\xc9\x95\xcf\x89???????)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterHP = { UE4CodeGen_Private::EPropertyClass::Float, "CharacterHP", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000020001, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, CharacterHP), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterHP_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterHP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_ReceivingCapsule_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "?_???[?W???\xf3\x82\xaf\x82??\xcd\x88?" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_ReceivingCapsule = { UE4CodeGen_Private::EPropertyClass::Object, "ReceivingCapsule", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, ReceivingCapsule), Z_Construct_UClass_UCapsuleComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_ReceivingCapsule_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_ReceivingCapsule_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseLookUpRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseLookUpRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseLookUpRate", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020015, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, BaseLookUpRate), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseLookUpRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseTurnRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseTurnRate", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020015, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_FollowCamera = { UE4CodeGen_Private::EPropertyClass::Object, "FollowCamera", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_FollowCamera_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FireEscapeCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CameraBoom = { UE4CodeGen_Private::EPropertyClass::Object, "CameraBoom", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AFireEscapeCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CameraBoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CameraBoom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFireEscapeCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_boolHealed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimePlus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeMinute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeSecond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_TimeDecimal1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_InitialHP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterPositionX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CharacterHP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_ReceivingCapsule,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseLookUpRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_FollowCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeCharacter_Statics::NewProp_CameraBoom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFireEscapeCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFireEscapeCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFireEscapeCharacter_Statics::ClassParams = {
		&AFireEscapeCharacter::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AFireEscapeCharacter_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::PropPointers),
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AFireEscapeCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFireEscapeCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFireEscapeCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFireEscapeCharacter, 1511854519);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFireEscapeCharacter(Z_Construct_UClass_AFireEscapeCharacter, &AFireEscapeCharacter::StaticClass, TEXT("/Script/FireEscape"), TEXT("AFireEscapeCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFireEscapeCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
