// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FireEscape/FireEscapeGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFireEscapeGameMode() {}
// Cross Module References
	FIREESCAPE_API UEnum* Z_Construct_UEnum_FireEscape_EFirePlayState();
	UPackage* Z_Construct_UPackage__Script_FireEscape();
	FIREESCAPE_API UClass* Z_Construct_UClass_AFireEscapeGameMode_NoRegister();
	FIREESCAPE_API UClass* Z_Construct_UClass_AFireEscapeGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond();
	FIREESCAPE_API UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	static UEnum* EFirePlayState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FireEscape_EFirePlayState, Z_Construct_UPackage__Script_FireEscape(), TEXT("EFirePlayState"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFirePlayState(EFirePlayState_StaticEnum, TEXT("/Script/FireEscape"), TEXT("EFirePlayState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FireEscape_EFirePlayState_CRC() { return 4051777612U; }
	UEnum* Z_Construct_UEnum_FireEscape_EFirePlayState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FireEscape();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFirePlayState"), 0, Get_Z_Construct_UEnum_FireEscape_EFirePlayState_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFirePlayState::EPlaying", (int64)EFirePlayState::EPlaying },
				{ "EFirePlayState::EGameOver", (int64)EFirePlayState::EGameOver },
				{ "EFirePlayState::EClear", (int64)EFirePlayState::EClear },
				{ "EFirePlayState::EUnknown", (int64)EFirePlayState::EUnknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "EClear.ToolTip", "?Q?[???I?[?o?[" },
				{ "EGameOver.ToolTip", "?Q?[???v???C??" },
				{ "EUnknown.ToolTip", "?Q?[???N???A" },
				{ "ModuleRelativePath", "FireEscapeGameMode.h" },
				{ "ToolTip", "?Q?[???X?e?[?g" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FireEscape,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EFirePlayState",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EFirePlayState",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AFireEscapeGameMode::StaticRegisterNativesAFireEscapeGameMode()
	{
		UClass* Class = AFireEscapeGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentState", &AFireEscapeGameMode::execGetCurrentState },
			{ "GetFinalTimeDecimal1", &AFireEscapeGameMode::execGetFinalTimeDecimal1 },
			{ "GetFinalTimeDecimal2", &AFireEscapeGameMode::execGetFinalTimeDecimal2 },
			{ "GetFinalTimeMinute", &AFireEscapeGameMode::execGetFinalTimeMinute },
			{ "GetFinalTimeSecond", &AFireEscapeGameMode::execGetFinalTimeSecond },
			{ "GetHPToOver", &AFireEscapeGameMode::execGetHPToOver },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics
	{
		struct FireEscapeGameMode_eventGetCurrentState_Parms
		{
			EFirePlayState ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Enum, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeGameMode_eventGetCurrentState_Parms, ReturnValue), Z_Construct_UEnum_FireEscape_EFirePlayState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::NewProp_ReturnValue_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeGameMode, "GetCurrentState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(FireEscapeGameMode_eventGetCurrentState_Parms), Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics
	{
		struct FireEscapeGameMode_eventGetFinalTimeDecimal1_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeGameMode_eventGetFinalTimeDecimal1_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::Function_MetaDataParams[] = {
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeGameMode, "GetFinalTimeDecimal1", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(FireEscapeGameMode_eventGetFinalTimeDecimal1_Parms), Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics
	{
		struct FireEscapeGameMode_eventGetFinalTimeDecimal2_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeGameMode_eventGetFinalTimeDecimal2_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::Function_MetaDataParams[] = {
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeGameMode, "GetFinalTimeDecimal2", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(FireEscapeGameMode_eventGetFinalTimeDecimal2_Parms), Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics
	{
		struct FireEscapeGameMode_eventGetFinalTimeMinute_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeGameMode_eventGetFinalTimeMinute_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::Function_MetaDataParams[] = {
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeGameMode, "GetFinalTimeMinute", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(FireEscapeGameMode_eventGetFinalTimeMinute_Parms), Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics
	{
		struct FireEscapeGameMode_eventGetFinalTimeSecond_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeGameMode_eventGetFinalTimeSecond_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::Function_MetaDataParams[] = {
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeGameMode, "GetFinalTimeSecond", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(FireEscapeGameMode_eventGetFinalTimeSecond_Parms), Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics
	{
		struct FireEscapeGameMode_eventGetHPToOver_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(FireEscapeGameMode_eventGetHPToOver_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFireEscapeGameMode, "GetHPToOver", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(FireEscapeGameMode_eventGetHPToOver_Parms), Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AFireEscapeGameMode_NoRegister()
	{
		return AFireEscapeGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AFireEscapeGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalTimeCheck_MetaData[];
#endif
		static void NewProp_FinalTimeCheck_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FinalTimeCheck;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalTimeMinute_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FinalTimeMinute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalTimeSecond_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FinalTimeSecond;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalTimeDecimal1_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FinalTimeDecimal1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalTimeDecimal2_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FinalTimeDecimal2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HUDWidgetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_HUDWidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClearZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClearZ;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClearY_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClearY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClearX_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClearX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HPToOver_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HPToOver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FireRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FireRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFireEscapeGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FireEscape,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AFireEscapeGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AFireEscapeGameMode_GetCurrentState, "GetCurrentState" }, // 1639802854
		{ &Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal1, "GetFinalTimeDecimal1" }, // 4138726575
		{ &Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeDecimal2, "GetFinalTimeDecimal2" }, // 1776670291
		{ &Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeMinute, "GetFinalTimeMinute" }, // 3519488367
		{ &Z_Construct_UFunction_AFireEscapeGameMode_GetFinalTimeSecond, "GetFinalTimeSecond" }, // 1515472618
		{ &Z_Construct_UFunction_AFireEscapeGameMode_GetHPToOver, "GetHPToOver" }, // 1843765391
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "FireEscapeGameMode.h" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?\xc5\x8fI???\xd4\x83`?F?b?N" },
	};
#endif
	void Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck_SetBit(void* Obj)
	{
		((AFireEscapeGameMode*)Obj)->FinalTimeCheck = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck = { UE4CodeGen_Private::EPropertyClass::Bool, "FinalTimeCheck", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AFireEscapeGameMode), &Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck_SetBit, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeMinute_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?\xc5\x8fI???\xd4\x8co??(??)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeMinute = { UE4CodeGen_Private::EPropertyClass::Int, "FinalTimeMinute", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeMinute), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeMinute_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeMinute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeSecond_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?\xc5\x8fI???\xd4\x8co??(?b)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeSecond = { UE4CodeGen_Private::EPropertyClass::Int, "FinalTimeSecond", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeSecond), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeSecond_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeSecond_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal1_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal1 = { UE4CodeGen_Private::EPropertyClass::Int, "FinalTimeDecimal1", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeDecimal1), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal1_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal2_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "FinalTime" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?\xc5\x8fI???\xd4\x81i?Q?[???I?????\xcc\x8e??\xd4\x8co?\xdf\x81j\n?\xc5\x8fI???\xd4\x8co??(?R???})" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal2 = { UE4CodeGen_Private::EPropertyClass::Int, "FinalTimeDecimal2", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, FinalTimeDecimal2), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal2_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_CurrentWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "HUD?\xcc\x83""C???X?^???X" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_CurrentWidget = { UE4CodeGen_Private::EPropertyClass::Object, "CurrentWidget", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000080008, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, CurrentWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_CurrentWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_CurrentWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HUDWidgetClass_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "HUD???\xca\x82\xc9\x95K?v??Widget?N???X" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HUDWidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "HUDWidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0024080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, HUDWidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HUDWidgetClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HUDWidgetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearZ_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?Q?[???N???A???\xe9\x8e\x9e?\xcc\x88\xca\x92u" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearZ = { UE4CodeGen_Private::EPropertyClass::Float, "ClearZ", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, ClearZ), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearZ_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearZ_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearY_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?Q?[???N???A???\xe9\x8e\x9e?\xcc\x88\xca\x92u" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearY = { UE4CodeGen_Private::EPropertyClass::Float, "ClearY", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, ClearY), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearY_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearX_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Position" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?Q?[???N???A???\xe9\x8e\x9e?\xcc\x88\xca\x92u" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearX = { UE4CodeGen_Private::EPropertyClass::Float, "ClearX", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, ClearX), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HPToOver_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "?Q?[???I?[?o?[???\xe9\x8e\x9e??HP" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HPToOver = { UE4CodeGen_Private::EPropertyClass::Float, "HPToOver", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, HPToOver), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HPToOver_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HPToOver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FireRate_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Health" },
		{ "ModuleRelativePath", "FireEscapeGameMode.h" },
		{ "ToolTip", "???\xf0\x97\x81\x82\xd1\x82\xc4\x82????\xd4\x82\xc9\x8c???????HP" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FireRate = { UE4CodeGen_Private::EPropertyClass::Float, "FireRate", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000010005, 1, nullptr, STRUCT_OFFSET(AFireEscapeGameMode, FireRate), METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FireRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FireRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFireEscapeGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeCheck,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeMinute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeSecond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FinalTimeDecimal2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_CurrentWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HUDWidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_ClearX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_HPToOver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFireEscapeGameMode_Statics::NewProp_FireRate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFireEscapeGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFireEscapeGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFireEscapeGameMode_Statics::ClassParams = {
		&AFireEscapeGameMode::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008802A8u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AFireEscapeGameMode_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AFireEscapeGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AFireEscapeGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFireEscapeGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFireEscapeGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFireEscapeGameMode, 156010168);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFireEscapeGameMode(Z_Construct_UClass_AFireEscapeGameMode, &AFireEscapeGameMode::StaticClass, TEXT("/Script/FireEscape"), TEXT("AFireEscapeGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFireEscapeGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
