// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIREESCAPE_HealHP_generated_h
#error "HealHP.generated.h already included, missing '#pragma once' in HealHP.h"
#endif
#define FIREESCAPE_HealHP_generated_h

#define FireEscape_Source_FireEscape_HealHP_h_12_RPC_WRAPPERS \
	virtual void WasHealed_Implementation(); \
 \
	DECLARE_FUNCTION(execWasHealed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->WasHealed_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetHealActive) \
	{ \
		P_GET_UBOOL(Z_Param_NewDamageState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetHealActive(Z_Param_NewDamageState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHealActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->HealActive(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_HealHP_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execWasHealed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->WasHealed_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetHealActive) \
	{ \
		P_GET_UBOOL(Z_Param_NewDamageState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetHealActive(Z_Param_NewDamageState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execHealActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->HealActive(); \
		P_NATIVE_END; \
	}


#define FireEscape_Source_FireEscape_HealHP_h_12_EVENT_PARMS
#define FireEscape_Source_FireEscape_HealHP_h_12_CALLBACK_WRAPPERS
#define FireEscape_Source_FireEscape_HealHP_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHealHP(); \
	friend struct Z_Construct_UClass_AHealHP_Statics; \
public: \
	DECLARE_CLASS(AHealHP, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AHealHP)


#define FireEscape_Source_FireEscape_HealHP_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAHealHP(); \
	friend struct Z_Construct_UClass_AHealHP_Statics; \
public: \
	DECLARE_CLASS(AHealHP, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FireEscape"), NO_API) \
	DECLARE_SERIALIZER(AHealHP)


#define FireEscape_Source_FireEscape_HealHP_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHealHP(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHealHP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHealHP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHealHP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHealHP(AHealHP&&); \
	NO_API AHealHP(const AHealHP&); \
public:


#define FireEscape_Source_FireEscape_HealHP_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHealHP(AHealHP&&); \
	NO_API AHealHP(const AHealHP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHealHP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHealHP); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AHealHP)


#define FireEscape_Source_FireEscape_HealHP_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HealMesh() { return STRUCT_OFFSET(AHealHP, HealMesh); }


#define FireEscape_Source_FireEscape_HealHP_h_9_PROLOG \
	FireEscape_Source_FireEscape_HealHP_h_12_EVENT_PARMS


#define FireEscape_Source_FireEscape_HealHP_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_HealHP_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_HealHP_h_12_RPC_WRAPPERS \
	FireEscape_Source_FireEscape_HealHP_h_12_CALLBACK_WRAPPERS \
	FireEscape_Source_FireEscape_HealHP_h_12_INCLASS \
	FireEscape_Source_FireEscape_HealHP_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FireEscape_Source_FireEscape_HealHP_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FireEscape_Source_FireEscape_HealHP_h_12_PRIVATE_PROPERTY_OFFSET \
	FireEscape_Source_FireEscape_HealHP_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_HealHP_h_12_CALLBACK_WRAPPERS \
	FireEscape_Source_FireEscape_HealHP_h_12_INCLASS_NO_PURE_DECLS \
	FireEscape_Source_FireEscape_HealHP_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FireEscape_Source_FireEscape_HealHP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
